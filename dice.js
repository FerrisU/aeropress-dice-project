var methodP = document.getElementById('method');
var ratioP = document.getElementById('ratio');
var grindP = document.getElementById('grind');
var tempP = document.getElementById('temp');
var bloomP = document.getElementById('bloom');
var timeP = document.getElementById('time');
var agitationP = document.getElementById('agitation');

//choices for all options
//Note: if you're editing these options for your own use:
//Make sure to put your options BEFORE the "Your choice" options.
const methodA = ["Standard", "Inverted", "Your choice! :)"];
const ratioA = ["1:13", "1:14", "1:15", "1:16", "1:17", "Your choice! :)"];
const grindA = ["Very fine", "Fine", "Medium", "Coarse", "Very coarse", "Your choice! :)"];
const tempA = ["80 celsius", "85 celsius", "90 celsius", "95 celsius", "Straight off boil", "Your choice! :)"];
const bloomA = ["Bloom", "No bloom", "Your choice! :)"];
const timeA = ["30 seconds", "1 minute", "1 minute 30 seconds", "2 minutes", "3 minutes", "5 minutes", "10 minutes", "Your choice! :)"];
const agitationA = ["No agitation", "Stir after pouring", "Stir after brew time", "Stir after pouring and after brew time is over", "Swirl after pouring", "Swirl after brew time", "Swirl after pouring and after brew time is over", "Your choice! :)"];

var method;
var ratio;
var grindSize;
var temperature;
var bloom;
var brewTime;
var agitate;

//select random options from arrays
function roll() {

    if(document.getElementById('yourChoiceBox').checked == true) {
        method = methodA[Math.floor(Math.random() * methodA.length)];
        ratio = ratioA[Math.floor(Math.random() * ratioA.length)];
        grindSize = grindA[Math.floor(Math.random() * grindA.length)];
        temperature = tempA[Math.floor(Math.random() * tempA.length)];
        bloom = bloomA[Math.floor(Math.random() * bloomA.length)];
        brewTime = timeA[Math.floor(Math.random() * timeA.length)];
        agitate = agitationA[Math.floor(Math.random() * agitationA.length)];
    }
    else {
        method = methodA[Math.floor(Math.random() * (methodA.length - 1))];
        ratio = ratioA[Math.floor(Math.random() * (ratioA.length - 1))];
        grindSize = grindA[Math.floor(Math.random() * (grindA.length - 1))];
        temperature = tempA[Math.floor(Math.random() * (tempA.length - 1))];
        bloom = bloomA[Math.floor(Math.random() * (bloomA.length - 1))];
        brewTime = timeA[Math.floor(Math.random() * (timeA.length - 1))];
        agitate = agitationA[Math.floor(Math.random() * (agitationA.length - 1))];
    }
    

    input();
}


//input the rolled options
function input() {

    methodP.innerHTML = method;
    ratioP.innerHTML = ratio;
    grindP.innerHTML = grindSize;
    tempP.innerHTML = temperature;
    bloomP.innerHTML = bloom;
    timeP.innerHTML = brewTime;
    agitationP.innerHTML = agitate;
}